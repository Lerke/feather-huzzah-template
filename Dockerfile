FROM ubuntu

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update -y
RUN apt-get install -y make unrar-free autoconf automake libtool gcc g++ gperf \
  flex bison texinfo gawk ncurses-dev libexpat-dev python-dev python python-serial \
  sed git unzip bash help2man wget bzip2
RUN apt-get install -y libtool-bin

RUN git clone https://github.com/pfalcon/esp-open-sdk
WORKDIR /esp-open-sdk

RUN echo "\nCT_DEBUG_gdb=n\n" >> "./crosstool-config-overrides"

RUN useradd ctngbuild
RUN chown -R ctngbuild .
RUN chmod -R u+rX .

USER ctngbuild
RUN make -j4 toolchain libhal STANDALONE=n

RUN make -j4 esptool STANDALONE=n
USER root
ENV PATH="/esp-open-sdk/xtensa-lx106-elf/bin/:${PATH}"

# Build app
WORKDIR /app 
ADD . /app

RUN git submodule init
RUN git submodule update 

CMD [ "/bin/bash" ]