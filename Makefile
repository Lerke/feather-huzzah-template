PROGRAM = AppName
PROGRAM_SRC_DIR=./src
PROGRAM_INC_DIR=./inc
ESPBAUD=921600

EXTRA_CFLAGS+=-std=gnu11

include ./external/esp-open-rtos/common.mk

PROGRAM_CFLAGS+=-O0