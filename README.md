# feather-huzzah-template

Template project for Adafruit Feather Huzzah! based on esp-open-rtos. Entry point of the application is `app.c:user_init()`.

## Prerequisites

- xtensa-lx106-gcc toolchain

## Build

```sh
$ make clean all
...
FW firmware/AppName.bin
esptool.py v1.2
```

## Flash

Use `make flash` to flash the binary to the board.

```sh
$ make flash
...
Wrote 253952 bytes at 0x2000 in 4.3 seconds (477.6 kbit/s)...
Leaving...
```