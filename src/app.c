#include "espressif/esp_common.h"
#include "esp/uart.h"

/**
 * App Entry
 */
void user_init()
{
  uart_set_baud(0, 115200);
  printf("SDK Version:%s\n", sdk_system_get_sdk_version());
}